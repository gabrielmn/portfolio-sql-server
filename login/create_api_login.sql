-- =============================================
-- Create API Login
-- =============================================
CREATE LOGIN [api_login] WITH PASSWORD = '${API_PASSWORD}';
GO
-- =============================================
-- Enable API login
-- =============================================
ALTER LOGIN [api_login] ENABLE;
GO
-- =============================================
-- Create API user
-- =============================================
CREATE USER [api_user] FOR LOGIN [api_login] WITH DEFAULT_SCHEMA = [portifolio];
GO
-- ====================================================
-- Grant API user permissions on certificate schema
-- ====================================================
GRANT SELECT ON SCHEMA :: [certificate] TO [api_user];
GO
-- ====================================================
-- Grant API user permissions on experience schema
-- ====================================================
GRANT SELECT ON SCHEMA :: [experience] TO [api_user];
GO
-- ====================================================
-- Grant API user permissions on project schema
-- ====================================================
GRANT SELECT ON SCHEMA :: [project] TO [api_user];
GO