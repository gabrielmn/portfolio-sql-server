import { createConnection, ConnectionPool, closeConnection, createRequest, Request  } from '../database_handler';

const LOGIN = "api_login";
const USER = "api_user";

describe('Login creation', () => {

    let connection: ConnectionPool;

    beforeAll(async () => {
        connection = await createConnection('portfolio');
    });

    afterAll(async () => {
        closeConnection(connection);
    });

    test(`Database login ${LOGIN} exists`, async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query(`SELECT [name] FROM [sys].[sql_logins] WHERE [name] = '${LOGIN}';`); 
        
        expect(rowsAffected[0]).toBe(1);
        expect(recordset[0].name).toBe(LOGIN);
    });

    test(`Database user ${USER} exists`, async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query(`SELECT [name] FROM [sys].[database_principals] WHERE [name] = '${USER}';`); 
        
        expect(rowsAffected[0]).toBe(1);
        expect(recordset[0].name).toBe(USER);
    });

});
