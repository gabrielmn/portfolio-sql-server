import { createConnection, ConnectionPool, closeConnection, createRequest, Request  } from '../database_handler';

describe('Database creation', () => {

    let connection: ConnectionPool;

    beforeAll(async () => {
        connection = await createConnection('master');
    });

    afterAll(async () => {
        closeConnection(connection);
    });

    test('Database portfolio exists', async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query("SELECT [name] FROM [sys].[databases] WHERE [name] = 'portfolio';"); 
        
        expect(rowsAffected[0]).toBe(1);
        expect(recordset[0].name).toBe('portfolio');
    });

});
