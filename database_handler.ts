import 'dotenv/config';
import { connect, Request, ConnectionPool, config, Transaction } from 'mssql';
// Re-export types from 'mssql'
export { BigInt, NVarChar, DateTime, Int, Decimal, Bit, Date, ConnectionPool, Request, Transaction } from 'mssql';

const server = process.env['MSSQL_SERVER'];
if (server === undefined)
    throw new Error('Environment variable server is not defined')


const sqlConfig: config = {

    server: server,
    user: process.env['MSSQL_SA_USER'],
    password: process.env['MSSQL_SA_PASSWORD'],
    connectionTimeout:20000, // remove later
    requestTimeout: 20000, // remove later
    options: {
        encrypt: true, // for azure
        trustServerCertificate: true // change to true for local dev / self-signed certs
    }
};

/**
 *
 * Establish a connection with the database. 
 *
 * @returns a new connection.
 */
export async function createConnection(database:string): Promise<ConnectionPool> {

    // Clone the sqlConfig to avoid mutating the original object
    const newConfig = { ...sqlConfig };

    // If a databaseName is provided, override the database property
    newConfig.database = database;

    // Establish and return the database connection
    return await connect(newConfig);
}

/**
 * 
 * End the connection with the database. 
 *
 * @param {*} connection - connection with the database.
 */
export function closeConnection(connection: ConnectionPool) {

    if (connection) {
        connection.close();
    }

}

export function createRequest(connection: ConnectionPool): Request {

    if(!connection)
        throw new Error('Connection must be established before creating a request.');
    
    return new Request(connection);
}

export function createTransaction(connection: ConnectionPool): Transaction{
    
    if(!connection)
        throw new Error('Connection must be established before creating a transaction.');
    
    return new Transaction(connection);
}
