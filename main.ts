import 'dotenv/config';
import { readFile, writeFile } from 'fs/promises';
import { join } from 'path';


async function main() {

    await createForDocker();
    await createForAzure();

}

async function createForDocker() {
 
    let script:string = '', temp:Buffer;
    
    // load database
    temp = await readFile(join(__dirname, './database/create_database.sql'));
    script += temp.toString();
    // load logins
    // project schema
    temp = await readFile(join(__dirname, './login/create_api_login.sql'));
    if(process.env["API_PASSWORD"])
        temp = Buffer.from(temp.toString('utf8').replace(new RegExp(`\\$\\{API_PASSWORD\\}`, 'g'), process.env["API_PASSWORD"]));
    script += '\n';
    script += temp.toString();
    // load schemas
    // project schema
    temp = await readFile(join(__dirname, './schema/create_project_schema.sql'));
    script += '\n';
    script += temp.toString();
    // certificate schema
    temp = await readFile(join(__dirname, './schema/create_certificate_schema.sql'));
    script += '\n';
    script += temp.toString();
    // experience schema
    temp = await readFile(join(__dirname, './schema/create_experience_schema.sql'));
    script += '\n';
    script += temp.toString();
    // load tables
    // project tables
    temp = await readFile(join(__dirname, './project/create_projects_table.sql'));
    script += '\n';
    script += temp.toString();
    temp = await readFile(join(__dirname, './project/create_project_descriptions_table.sql'));
    script += '\n';
    script += temp.toString();
    temp = await readFile(join(__dirname, './project/create_project_tags_table.sql'));
    script += '\n';
    script += temp.toString();
    // certificate tables
    temp = await readFile(join(__dirname, './certificate/create_certificates_table.sql'));
    script += '\n';
    script += temp.toString();
    temp = await readFile(join(__dirname, './certificate/create_certificate_topics_table.sql'));
    script += '\n';
    script += temp.toString();
    // experience tables
    // certificate schema
    temp = await readFile(join(__dirname, './experience/create_experiences_table.sql'));
    script += '\n';
    script += temp.toString();
    temp = await readFile(join(__dirname, './experience/create_experience_responsibilities_table.sql'));
    script += '\n';
    script += temp.toString();

    writeFile(join(__dirname,'./create_database.docker.sql'), script);

}


async function createForAzure() {

    let script:string = '', temp:Buffer;
        
    writeFile(join(__dirname,'./create_database.azure.sql'), script);
}

main();
