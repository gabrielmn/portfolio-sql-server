-- =============================================
-- Create project descriptions table
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [project].[project_descriptions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
    [project_id] [bigint] NOT NULL,
    [description] [nvarchar] (2048) NOT NULL,
    [language] [nvarchar] (8) NOT NULL,
    CONSTRAINT [PK_project_descriptions] PRIMARY KEY CLUSTERED ([id] ASC) 
    WITH (
        PAD_INDEX = OFF, 
        STATISTICS_NORECOMPUTE = OFF, 
        IGNORE_DUP_KEY = OFF, 
        ALLOW_ROW_LOCKS = ON, 
        ALLOW_PAGE_LOCKS = ON, 
        OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
    ) ON [PRIMARY],
    CONSTRAINT [FK_project_description_project_id] FOREIGN KEY([project_id]) REFERENCES [project].[projects] ([id]) ON DELETE CASCADE,
    CONSTRAINT [UQ_project_descriptions_project_id_description_language] UNIQUE ([project_id], [description], [language])
)
GO