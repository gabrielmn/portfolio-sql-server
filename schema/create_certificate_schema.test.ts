import { createConnection, ConnectionPool, closeConnection, createRequest, Request  } from '../database_handler';

const DATABASE =  'portfolio';
const SCHEMA = 'certificate';

describe(`Schema ${SCHEMA}`, () => {

    let connection: ConnectionPool;

    beforeAll(async () => {
        connection = await createConnection(DATABASE);
    });

    afterAll(async () => {
        closeConnection(connection);
    });

    test(`Schema ${SCHEMA} exists`, async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query(`SELECT [SCHEMA_NAME] FROM [INFORMATION_SCHEMA].[SCHEMATA] WHERE [CATALOG_NAME] = '${DATABASE}' AND [SCHEMA_NAME] = '${SCHEMA}';`); 
        
        expect(rowsAffected[0]).toBe(1);
        expect(recordset[0].SCHEMA_NAME).toBe(SCHEMA);
    });

});
