#!/bin/bash
set -e

# Start SQL Server in the background
/opt/mssql/bin/sqlservr &

# Wait for SQL Server to be ready
echo "Waiting for SQL Server to be ready..."
until /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "$MSSQL_SA_PASSWORD" -Q "SELECT 1" &> /dev/null
do
    echo -n "."
    sleep 1
done
echo -e "\nSQL Server is ready."

# Execute the SQL script
echo "Executing SQL script..."
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "$MSSQL_SA_PASSWORD" -i /usr/src/app/create_database.docker.sql

# Keep the container running
echo "SQL Server initialization is complete. Container is running."
tail -f /dev/null
