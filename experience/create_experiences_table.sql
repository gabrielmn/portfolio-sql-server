-- =============================================
-- Create experiences table
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [experience].[experiences](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
    [type] [nvarchar](32) NOT NULL,
    [time] [nvarchar](32) NOT NULL,
    [company] [nvarchar] (256) NOT NULL,
    [position] [nvarchar] (256) NOT NULL,
    [start_date] [date] NOT NULL,
    [end_date] [date] NULL,
    [image] [nvarchar] (2048) NOT NULL,
    CONSTRAINT [PK_experiences] PRIMARY KEY CLUSTERED ([id] ASC) 
    WITH (
        PAD_INDEX = OFF, 
        STATISTICS_NORECOMPUTE = OFF, 
        IGNORE_DUP_KEY = OFF, 
        ALLOW_ROW_LOCKS = ON, 
        ALLOW_PAGE_LOCKS = ON, 
        OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
    ) ON [PRIMARY],
    CONSTRAINT [CK_experiences_type] CHECK ([type] IN ('Hobby', 'Internship', 'Regular', 'Consulting')),
    CONSTRAINT [CK_experiences_time] CHECK ([time] IN ('Part-Time', 'Full-Time'))
)
GO
