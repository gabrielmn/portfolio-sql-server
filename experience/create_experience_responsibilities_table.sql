-- =============================================
-- Create experience responsibilities table
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [experience].[experience_responsibilities](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
    [experience_id] [bigint] NOT NULL,
    [responsibility] [nvarchar] (64) NOT NULL,
    [language] [nvarchar] (8) NOT NULL,
    CONSTRAINT [PK_experience_responsibilities] PRIMARY KEY CLUSTERED ([id] ASC) 
    WITH (
        PAD_INDEX = OFF, 
        STATISTICS_NORECOMPUTE = OFF, 
        IGNORE_DUP_KEY = OFF, 
        ALLOW_ROW_LOCKS = ON, 
        ALLOW_PAGE_LOCKS = ON, 
        OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
    ) ON [PRIMARY],
    CONSTRAINT [FK_experience_responsibilities_experience_id] FOREIGN KEY([experience_id]) REFERENCES [experience].[experiences] ([id]) ON DELETE CASCADE,
    CONSTRAINT [UQ_experience_responsibilities_experience_id_responsibility_language] UNIQUE ([experience_id], [responsibility], [language])
)
GO