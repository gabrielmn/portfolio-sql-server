# Contributing to Spotit website

#### Table Of Contents

[Guidelines](#guidelines)
  * [Git Commit Messages](#git-commit-messages)
  * [Changelog](#changelog)

## Guidelines

### Git Commit Messages 
##### guidelines base on [angular guidelines](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#commit)

Due to the use of [commitlint](https://commitlint.js.org/#/) and [husky](https://typicode.github.io/husky/#/), this project has very precise rules over how our git commit messages can be formatted. This leads to more readable messages that are easy to follow when looking through the project history. On top of that, the git commits are used to automatically generate the [CHANGELOG](CHANGELOG.md).

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier
to read.

The footer should contain a closing reference to an issue if any.


### Revert
If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.
```
revert: add commmitlint and husky

This reverts the following commit:  c2a521a54353fe387eeaa00967901f82c1dde915
```
### Type
Must be one of the following:

* **build**: Changes that affect the build system or external dependencies
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **style**: Changes that do not affect the meaning of the code
* **test**: Adding missing tests or correcting existing tests

### Scope
The scope should be the part of the project affected by the change.

The following is the list of supported scopes:

* **build**
    * **git**: Changes that affect the git
    * **npm**: Changes to the dependency list
* **docs**
    * **changelog**: Changes to the CHANGELOG file
    * **contributing**: Changes to the CONTRIBUING file
* **feat**, **fix**, **pref**, **refactor**, **style** and **test**
    * **establishment**
    * **security**
    * **user**
    
### Subject
The subject contains a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to reference issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

### Changelog
##### guidelines based on [standard-version](https://github.com/conventional-changelog/standard-version)
This project follow the [SemVer](https://semver.org/) guidelines and it's automatically generated with the following commands.


### First Release

To generate your changelog for your first release, simply do:

```sh
npm run release:first-release
```

### Cutting Releases

If you typically use `npm version` to cut a new release, do this instead:

```sh
npm run release
```

As long as your git commit messages are conventional and accurate, you no longer need to specify the semver type - and you get CHANGELOG generation for free! \o/

### Release as a Pre-Release

Use the flag `--prerelease` to generate pre-releases:

Suppose the last version of your code is `1.0.0`, and your code to be committed has patched changes. Run:

```bash
npm run release:prerelease
```
This will tag your version as: `1.0.1-0`.

If you want to name the pre-release, you specify the name via `--prerelease <name>`.

For example, suppose your pre-release should contain the `alpha` prefix:

```bash
# npm run script
npm run release:prerelease alpha
```

This will tag the version as: `1.0.1-alpha.0`

### Release as a Target Type Imperatively (`npm version`-like)

To forgo the automated version bump use `--release-as` with the argument `major`, `minor` or `patch`.

Suppose the last version of your code is `1.0.0`, you've only landed `fix:` commits, but
you would like your next release to be a `minor`. Simply run the following:

```bash
npm run release:minor
# or
npm run release:patch
# or
npm run release:major
```

You will get version `1.1.0` rather than what would be the auto-generated version `1.0.1`.