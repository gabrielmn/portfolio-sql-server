-- =============================================
-- Create certificates table
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [certificate].[certificates](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
    [name] [nvarchar] (256) NOT NULL,
    [conclusion_date] [date] NOT NULL,
    [link] [nvarchar] (2048) NOT NULL,
    CONSTRAINT [PK_certificates] PRIMARY KEY CLUSTERED ([id] ASC) 
    WITH (
        PAD_INDEX = OFF, 
        STATISTICS_NORECOMPUTE = OFF, 
        IGNORE_DUP_KEY = OFF, 
        ALLOW_ROW_LOCKS = ON, 
        ALLOW_PAGE_LOCKS = ON, 
        OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
    ) ON [PRIMARY]
)
GO
