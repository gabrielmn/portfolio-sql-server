import { createConnection, ConnectionPool, closeConnection, createRequest, Request  } from '../database_handler';

const SCHEMA = 'certificate';
const TABLE_NAME =  'certificates';
const COLUMNS = [
    'id',
    'name',
    'conclusion_date', 
    'link'
]

describe(`Table ${TABLE_NAME}`, () => {

    let connection: ConnectionPool;

    beforeAll(async () => {
        connection = await createConnection('portfolio');
    });

    afterAll(async () => {
        closeConnection(connection);
    });

    test(`Table ${TABLE_NAME} exists`, async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query(`SELECT [TABLE_NAME] FROM [INFORMATION_SCHEMA].[TABLES] WHERE [TABLE_SCHEMA] = '${SCHEMA}' AND [TABLE_NAME] =  '${TABLE_NAME}' ;`); 
        
        expect(rowsAffected[0]).toBe(1);
        expect(recordset[0].TABLE_NAME).toBe(TABLE_NAME);
        
    });

    test('Table structure', async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query(`SELECT [COLUMN_NAME] FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE [TABLE_NAME] = '${TABLE_NAME}';`); 
        
        expect(rowsAffected[0]).toBe(COLUMNS.length);
        COLUMNS.forEach((column, index, array)=>{
            expect(recordset[index].COLUMN_NAME).toBe(column);    
        })
        
    });
});
