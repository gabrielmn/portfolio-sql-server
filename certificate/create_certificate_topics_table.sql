-- =============================================
-- Create certificate topics table
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [certificate].[certificate_topics](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
    [certificate_id] [bigint] NOT NULL,
    [topic] [nvarchar] (2048) NOT NULL,
    [language] [nvarchar] (8) NOT NULL,
    CONSTRAINT [PK_certificate_descriptions] PRIMARY KEY CLUSTERED ([id] ASC) 
    WITH (
        PAD_INDEX = OFF, 
        STATISTICS_NORECOMPUTE = OFF, 
        IGNORE_DUP_KEY = OFF, 
        ALLOW_ROW_LOCKS = ON, 
        ALLOW_PAGE_LOCKS = ON, 
        OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
    ) ON [PRIMARY],
    CONSTRAINT [FK_certificate_topics_certificate_id] FOREIGN KEY([certificate_id]) REFERENCES [certificate].[certificates] ([id]) ON DELETE CASCADE
)
GO
-- =============================================
-- Create certificate topics trigger before insert
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [certificate].[certificate_topics_before_insert_trigger] 
   ON  [certificate].[certificate_topics] 
   INSTEAD OF INSERT
AS 
BEGIN
    IF EXISTS (
        SELECT 1
        FROM [certificate].[certificate_topics]
        JOIN [inserted] 
        ON 
            [certificate].[certificate_topics].[certificate_id] = [inserted].[certificate_id]
            AND 
            [certificate].[certificate_topics].[topic] = [inserted].[topic]
            AND 
            [certificate].[certificate_topics].[language] = [inserted].[language]
    )
    BEGIN
        RAISERROR ('A row with the same certificate_id, topic, and language already exists.', 16, 1);
        ROLLBACK TRANSACTION;
    END
    ELSE
    BEGIN
        INSERT INTO [certificate].[certificate_topics] ([certificate_id], [topic], [language])
        SELECT [certificate_id], [topic], [language] FROM [inserted];
    END
END
GO

ALTER TABLE [certificate].[certificate_topics] ENABLE TRIGGER [certificate_topics_before_insert_trigger]
GO